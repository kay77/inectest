import unittest
import HtmlTestRunner
from selenium import webdriver
from selenium.webdriver.common.by import By
from Pageobjectclass import FillingINEC

class Demoselect(unittest.TestCase):
    BrowserURL = "https://cvr.inecnigeria.org/pu"
    driver = webdriver.Chrome()
    locatorGD = "//*[@id='SearchIndexForm']/div[6]/input"
    # //input[@class="btn block btn-success btn-block"]
    value1 = '30'
    value2 ='622'
    value3 ='7137'
    value4 ='98347'
    
    @classmethod
    def setUpClass(cls):
        cls.driver.get(cls.BrowserURL)  # openning url
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(7)
        
    def test_StateINECFORM(self):
        INEC= FillingINEC(self.driver)
        INEC.test_StateINECFORM(self.value1)
        INEC.test_LGAINECFORM(self.value2)
        INEC.test_RAINECFORM(self.value3)
        INEC.test_PUINECFORM(self.value4)
        INEC.test_directionINECFORM(self.locatorGD)
          
    @classmethod   
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'], exit=False)
