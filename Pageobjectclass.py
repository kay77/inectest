from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By

class FillingINEC():
    # Variables for insertion
    locatorstate1= "data[Search][state_id]"
    locatorstate2="//*[@id='SearchStateId']/option"
    locatorLGA1="//*[@id='SearchLocalGovernmentId"
    locatorLGA2="//*[@id='SearchLocalGovernmentId/option"
    locatorRA1="//*[@id='SearchRegistrationAreaId']"
    locatorRA2="//*[@id='SearchRegistrationAreaId/option']"
    locatorPU1="//*[@id='SearchPollingUnitId']"
    locatorPU2="//*[@id='SearchPollingUnitId']/option"
    
    def __init__(self, driver):
        self.driver=driver
    
    def test_StateINECFORM(self, value1):
        Parent_handle=self.driver.current_window_handle
        print(Parent_handle)
        dropdownStates = self.driver.find_element(By.NAME, self.locatorstate1)
        lista=self.driver.find_elements(By.XPATH,self.locatorstate2)
        print('The number of states for the election is:',len(lista))
        dd = Select(dropdownStates)
        dd.select_by_value(value1)
        for x in lista:
            if value1 in x.get_attribute('value'):
                print(x.text)
                break
        
        
        # Selection of LGA
    def test_LGAINECFORM(self, value2):
        dropdownLGA = self.driver.find_element(By.XPATH, self.locatorLGA1)
        listb=self.driver.find_elements(By.XPATH,self.locatorLGA2)
        print('The number of local govermernment for the election is :',len(listb))
        ddLGA = Select(dropdownLGA)
        ddLGA.select_by_value(value2)
        for a in listb:
            if value2 in a.get_attribute('value'):
                print(a.text)
                break
        
        
        # Selection of Registration area
    def test_RAINECFORM(self, value3):
        dropdownRA = self.driver.find_element(By.XPATH, self.locatorRA1)
        listc=self.driver.find_elements(By.XPATH,self.locatorRA2)
        print('The number of Registration area for the election is :',len(listc))
        ddRA = Select(dropdownRA)
        ddRA.select_by_value(value3)
        for b in listc:
            if value3 in b.get_attribute('value'):
                print(b.text)
                break
        
        
        
        # Selection of Pulling unit
    def test_PUINECFORM(self, value4):
        dropdownPU = self.driver.find_element(By.XPATH, self.locatorPU1).click()
        listd=self.driver.find_elements(By.XPATH, self.locatorPU2)
        print('The number of Pulling unit for the election is :',len(listd))
        dda = Select(dropdownPU)
        dda.select_by_value(value4)
        for c in listd:
            if value4 in c.get_attribute('value'):
                print(c.text)
                break
        
        
    def test_directionINECFORM(self, locatorGD):
        Getdirection = self.driver.find_element(By.XPATH, locatorGD)
        Getdirection.submit()
        print(self.driver.title)
        print(self.driver.current_url)
        self.driver.save_screenshot('my2.png')  