import unittest
import HtmlTestRunner
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class INECselect(unittest.TestCase):
        # Selection of States
    def setUp(self):
        self.driver = webdriver.Chrome()
        
    def test_homepage(self):
        driver = self.driver
        driver.get('https://cvr.inecnigeria.org/pu')  # openning url 
        self.assertEqual("Polling Unit Locator",driver.title,"webpage title does not match")
        
    def test_INECFORM(self):
        driver = self.driver
        driver.implicitly_wait(7)
        driver.get('https://cvr.inecnigeria.org/pu')  # openning url
        print(driver.title)
        dropdownStates = driver.find_element(By.NAME,"data[Search][state_id]")
        lista=driver.find_elements(By.XPATH,"//*[@id='SearchStateId']/option")
        print(' The number of states for the election is:',len(lista))
        dd = Select(dropdownStates)
        dd.select_by_value("30")
        for x in lista:
            if '30' in x.get_attribute('value'):
                print(x.text)
                break
        
        
        # Selection of LGA
        dropdownLGA = driver.find_element(By.XPATH,"//*[@id='SearchLocalGovernmentId']")
        listb=driver.find_elements(By.XPATH,"//*[@id='SearchLocalGovernmentId']/option")
        print(' The number of local govermernment for the election is :',len(listb))
        ddLGA = Select(dropdownLGA)
        ddLGA.select_by_value("622")
        for a in listb:
            if '622' in a.get_attribute('value'):
                print(a.text)
                break
        
        
        # Selection of Registration area
        dropdownRA = driver.find_element(By.XPATH,"//*[@id='SearchRegistrationAreaId']")
        listc=driver.find_elements(By.XPATH, "//*[@id='SearchRegistrationAreaId']/option")
        print(' The number of Registration area for the election is :',len(listc))
        ddRA = Select(dropdownRA)
        ddRA.select_by_value("7137") 
        for b in listc:
            if '7137' in b.get_attribute('value'):
                print(b.text)
                break
        
        # Selection of Pulling unit
        dropdownPU = driver.find_element(By.XPATH,"//*[@id='SearchPollingUnitId']")
        listd=driver.find_elements(By.XPATH,"//*[@id='SearchPollingUnitId']/option")
        print(' The number of Pulling unit for the election is :',len(listd))
        dda = Select(dropdownPU)
        dda.select_by_value("98347")
        for c in listd:
            if '98347' in c.get_attribute('value'):
                print(c.text)
                break
        
        
        wait = WebDriverWait(driver, 10)
        element = wait.until(EC.element_to_be_clickable((By.XPATH,"//input[@class='btn block btn-success btn-block']")))
        element.click()
        time.sleep(7)
        driver.save_screenshot('my2.png')
        time.sleep(7)
    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'], exit=False)
